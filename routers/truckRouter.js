const express = require('express');
const router = new express.Router();
const authMiddlware = require('../middlewares/authMiddlware');
const {Truck} = require('../models/truckModel');

router.get('/', authMiddlware, async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id}, {__v: 0});
  res.status(200).json({trucks});
});

router.post('/', authMiddlware, async (req, res) => {
  const truck = new Truck({
    created_by: req.user._id,
    type: req.body.type,
  });
  await truck.save();
  res.status(200).json({message: 'Truck created successfully'});
});

router.get('/:id', authMiddlware, async (req, res) => {
  const user = req.user;
  if (user) {
    const truck = await Truck.findOne({_id: req.params.id}, {__v: 0});
    res.status(200).json({truck: truck});
  } else {
    res.status(400).json({message: 'No authorized!'});
  }
});

router.put('/:id', authMiddlware, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const _id = req.params.id;

      const truck = await Truck.findOne({_id, created_by: req.user._id});

      if (!truck) {
        res.status(400).json({message: 'No truck'});
      }
      if (!truck.assigned_to) {
        truck.type = req.body.type;
        await truck.save();
        res.status(200).json({message: 'Truck changed successfully'});
      }
    } else {
      res.status(400).json({message: 'You are not a driver'});
    }
  } else {
    res.status(400).json({message: 'No authorized!'});
  }
});

router.delete('/:id', authMiddlware, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'DRIVER') {
      const _id = req.params.id;

      const truck = await Truck.findOne({_id, created_by: req.user._id});

      if (!truck) {
        res.status(400).json({message: 'No such truck'});
      }
      if (!truck.assigned_to) {
        await Truck.findOneAndDelete({
          _id,
          created_by: req.user._id,
        });
        res.status(200).json({message: 'Truck deleted'});
      }
    } else {
      res.status(400).json({message: 'You are not a driver'});
    }
  } else {
    res.status(400).json({message: 'No authorized!'});
  }
});

module.exports = router;
