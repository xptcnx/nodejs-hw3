const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  if (email && password && role) {
    let user = await User.findOne({email});
    if (user) {
      res.status(400).json({message: 'User is already exist!'});
    } else {
      user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role,
      });
      const token = jwt.sign({email: user.email, _id: user._id}, JWT_SECRET);

      user.tokens = user.tokens.concat({token});

      try {
        await user.save();
      } catch (error) {
        res.status(400).json({message: error.message});
      }

      res.status(200).json({message: 'Profile created successfully'});
    }
  } else {
    res.status(400).json({message: 'Enter credentials'});
  }
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});
  if (!email && !password) {
    res.status(400).json({message: 'Enter your credentials!'});
  }

  if (!user) {
    return res
        .status(400)
        .json({message: `No user with email '${email}' found!`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({email: user.email, _id: user._id}, JWT_SECRET);

  user.tokens = user.tokens.concat({token});
  await user.save();
  res.status(200).json({jwt_token: token});
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;
  const user = await User.findOne({email});
  if (!user) {
    return res
        .status(400)
        .json({message: `No user with email '${email}' found!`});
  }
  if (!email) {
    return res.status(400).json({message: `Enter your email`});
  }
  res.status(200).json({message: 'New password sent to your email address'});
};
