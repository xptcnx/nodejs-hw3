const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
    role: Joi.string().pattern(new RegExp('(SHIPPER|DRIVER)$')).required(),
  });

  await schema
      .validateAsync(req.body).then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};
