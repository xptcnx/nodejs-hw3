const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');
const {JWT_SECRET} = require('../config');

const authMiddlware = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(400)
        .json({message: `No authorized`});
  }
  const [, token] = header.split(' ');

  const decoded = jwt.verify(token, JWT_SECRET);

  const user = await User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
  });

  if (!user) {
    res.status(400).json({message: 'No authorized!'});
  }
  req.token = token;
  req.user = user;
  next();
};

module.exports = authMiddlware;
