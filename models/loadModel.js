const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    required: true,
  },
  assigned_to: {
    default: null,
  },
  state: {
    type: String,
    default: 'En route to Pick Up',
  },
  status: {
    type: String,
    default: 'NEW',
    trim: true,
  },
  name: {
    type: String,
    required: true,
    trim: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
    trim: true,
  },
  delivery_address: {
    type: String,
    required: true,
    trim: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
});

module.exports.Load = mongoose.model('Load', loadSchema);
