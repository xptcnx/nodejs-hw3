const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    uniqe: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  tokens: [
    {
      token: {
        type: String,
      },
    },
  ],
});
userSchema.virtual('trucks', {
  ref: 'Truck',
  localField: '_id',
  foreignField: 'created_by',
});

userSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'created_by',
});

module.exports.User = mongoose.model('User', userSchema);
